# Chinese (Traditional) translation for Freeciv
# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the Freeciv package.
# Zhang Xiaowei <zero000072@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: freeciv 2.4.99-dev\n"
"Report-Msgid-Bugs-To: https://www.hostedredmine.com/projects/freeciv\n"
"POT-Creation-Date: 2018-07-21 22:46+0100\n"
"PO-Revision-Date: 2015-03-30 14:26+0800\n"
"Last-Translator: Zhang Xiaowei <zero00072@gmail.com>\n"
"Language-Team: Chinese (Traditional) <zero00072@gmail.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: tools/ruledit/requirers_dlg.cpp:43
msgid "Close"
msgstr "關閉"

#. TRANS: %s could be any of a number of ruleset items (e.g., tech,
#. * unit type, ...
#: tools/ruledit/requirers_dlg.cpp:68
#, c-format
msgid "Needed by %s"
msgstr "%s需要"

#: tools/ruledit/ruledit.cpp:135
msgid "Print a summary of the options"
msgstr "顯示選項的摘要"

#: tools/ruledit/ruledit.cpp:137
msgid "Print the version number"
msgstr "顯示版本號碼"

#. TRANS: argument (don't translate) VALUE (translate)
#: tools/ruledit/ruledit.cpp:140
msgid "ruleset RULESET"
msgstr ""

#: tools/ruledit/ruledit.cpp:141
#, fuzzy
#| msgid "Give ruleset to use as starting point."
msgid "Ruleset to use as the starting point."
msgstr "設為劇本的起點。"

#: tools/ruledit/ruledit.cpp:154
msgid "Can only edit one ruleset at a time.\n"
msgstr ""

#: tools/ruledit/ruledit.cpp:161
#, c-format
msgid "Unrecognized option: \"%s\"\n"
msgstr "未確認的選項：「%s」\n"

#: tools/ruledit/ruledit_qt.cpp:115
#, c-format
msgid ""
"%s%s\n"
"commit: %s"
msgstr ""
"%s%s\n"
"提交：%s"

#: tools/ruledit/ruledit_qt.cpp:126
msgid "Give ruleset to use as starting point."
msgstr "設為劇本的起點。"

#: tools/ruledit/ruledit_qt.cpp:138
msgid "Start editing"
msgstr "開始編輯"

#: tools/ruledit/ruledit_qt.cpp:148
msgid "Misc"
msgstr "雜項"

#: tools/ruledit/ruledit_qt.cpp:150
msgid "Tech"
msgstr "科技"

#: tools/ruledit/ruledit_qt.cpp:152
msgid "Buildings"
msgstr "建築"

#: tools/ruledit/ruledit_qt.cpp:154
msgid "Units"
msgstr "單位"

#: tools/ruledit/ruledit_qt.cpp:156
msgid "Nations"
msgstr "國族"

#: tools/ruledit/ruledit_qt.cpp:165
msgid "Welcome to freeciv-ruledit"
msgstr "歡迎來到 Freeciv 劇本編輯器"

#: tools/ruledit/ruledit_qt.cpp:185
msgid "Ruleset loaded"
msgstr "已載入劇本"

#: tools/ruledit/ruledit_qt.cpp:201
msgid "Ruleset loading failed!"
msgstr "載入劇本失敗！"

#: tools/ruledit/ruledit_qt.cpp:245
msgid "Freeciv Ruleset Editor"
msgstr "Freeciv 劇本編輯器"

#: tools/ruledit/ruledit_qt.cpp:261
msgid "Are you sure you want to quit?"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:265
msgid "Quit?"
msgstr ""

#: tools/ruledit/tab_building.cpp:61 tools/ruledit/tab_tech.cpp:61
#: tools/ruledit/tab_unit.cpp:61
msgid "Rule Name"
msgstr "規則名稱"

#: tools/ruledit/tab_building.cpp:69 tools/ruledit/tab_tech.cpp:69
#: tools/ruledit/tab_unit.cpp:69
msgid "Name"
msgstr "名稱"

#: tools/ruledit/tab_building.cpp:152
#, fuzzy
#| msgid "Building with that rule name already exist!"
msgid "A building with that rule name already exists!"
msgstr "已存在以此命名的建築！"

#: tools/ruledit/tab_misc.cpp:61
msgid "Ruleset name"
msgstr "劇本名稱"

#: tools/ruledit/tab_misc.cpp:66
msgid "Ruleset version"
msgstr "劇本版本"

#: tools/ruledit/tab_misc.cpp:71
msgid "Save to directory"
msgstr "儲存至目錄"

#: tools/ruledit/tab_misc.cpp:78
msgid "Save now"
msgstr "立即儲存"

#: tools/ruledit/tab_misc.cpp:85
#, fuzzy
#| msgid "Terrains"
msgid "?stat:Terrains"
msgstr "地形"

#: tools/ruledit/tab_misc.cpp:89
#, fuzzy
#| msgid "Resources"
msgid "?stat:Resources"
msgstr "資源"

#: tools/ruledit/tab_misc.cpp:93
#, fuzzy
#| msgid "Techs"
msgid "?stat:Techs"
msgstr "科技"

#: tools/ruledit/tab_misc.cpp:97
#, fuzzy
#| msgid "Unit Classes"
msgid "?stat:Unit Classes"
msgstr "單位類別"

#: tools/ruledit/tab_misc.cpp:101
#, fuzzy
#| msgid "Unit Types"
msgid "?stat:Unit Types"
msgstr "單位類型"

#: tools/ruledit/tab_misc.cpp:105
#, fuzzy
#| msgid "Buildings"
msgid "?stat:Buildings"
msgstr "建築"

#: tools/ruledit/tab_misc.cpp:109
#, fuzzy
#| msgid "Nations"
msgid "?stat:Nations"
msgstr "國族"

#: tools/ruledit/tab_misc.cpp:113
#, fuzzy
#| msgid "Styles"
msgid "?stat:Styles"
msgstr "樣式"

#: tools/ruledit/tab_misc.cpp:117
#, fuzzy
#| msgid "Specialists"
msgid "?stat:Specialists"
msgstr "專家"

#: tools/ruledit/tab_misc.cpp:121
#, fuzzy
#| msgid "Governments"
msgid "?stat:Governments"
msgstr "政府"

#: tools/ruledit/tab_misc.cpp:125
#, fuzzy
#| msgid "Disasters"
msgid "?stat:Disasters"
msgstr "天災"

#: tools/ruledit/tab_misc.cpp:129
#, fuzzy
#| msgid "Achievements"
msgid "?stat:Achievements"
msgstr "成就"

#: tools/ruledit/tab_misc.cpp:133
#, fuzzy
#| msgid "Extras"
msgid "?stat:Extras"
msgstr "額外"

#: tools/ruledit/tab_misc.cpp:137
#, fuzzy
#| msgid "Bases"
msgid "?stat:Bases"
msgstr "基礎"

#: tools/ruledit/tab_misc.cpp:141
#, fuzzy
#| msgid "Roads"
msgid "?stat:Roads"
msgstr "道路"

#: tools/ruledit/tab_misc.cpp:145
#, fuzzy
#| msgid "Styles"
msgid "?stat:Multipliers"
msgstr "樣式"

#: tools/ruledit/tab_misc.cpp:202
msgid "Ruleset saved"
msgstr "已儲存劇本"

#: tools/ruledit/tab_nation.cpp:49
msgid "Use nationlist"
msgstr "使用國族列表"

#: tools/ruledit/tab_nation.cpp:53
msgid "Nationlist"
msgstr "國族列表"

#: tools/ruledit/tab_tech.cpp:80
msgid "Req1"
msgstr "需求一"

#: tools/ruledit/tab_tech.cpp:89
msgid "Req2"
msgstr "需求二"

#: tools/ruledit/tab_tech.cpp:97
msgid "Root Req"
msgstr "基本需求"

#: tools/ruledit/tab_tech.cpp:182
msgid "Never"
msgstr "從未"

#: tools/ruledit/tab_tech.cpp:318
#, fuzzy
#| msgid "Tech with that rule name already exist!"
msgid "A tech with that rule name already exists!"
msgstr "已存在以此命名的科技！"

#: tools/ruledit/tab_unit.cpp:152
#, fuzzy
#| msgid "Unit type with that rule name already exist!"
msgid "A unit type with that rule name already exists!"
msgstr "已存在以此命名的單位類型！"

#: tools/ruledit/validity.c:64
msgid "Effect"
msgstr "有效"

#: tools/ruledit/validity.c:125
msgid "Action Enabler"
msgstr "行為啟用者"

#: tools/ruledit/validity.c:145
msgid "Music Style"
msgstr "音樂風格"

#~ msgid "Add Building"
#~ msgstr "加入建築"

#~ msgid "Remove this Building"
#~ msgstr "移除這個建築"

#~ msgid "Refresh Stats"
#~ msgstr "更新狀態"

#~ msgid "Add tech"
#~ msgstr "加入科技"

#~ msgid "Remove this tech"
#~ msgstr "移除這項科技"

#~ msgid "Add Unit"
#~ msgstr "加入單位"

#~ msgid "Remove this Unit"
#~ msgstr "移除這個單位"
