# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Freeciv Project
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: freeciv 2.6.0\n"
"Report-Msgid-Bugs-To: https://www.hostedredmine.com/projects/freeciv\n"
"POT-Creation-Date: 2018-07-21 22:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: tools/ruledit/requirers_dlg.cpp:43
msgid "Close"
msgstr ""

#. TRANS: %s could be any of a number of ruleset items (e.g., tech,
#. * unit type, ...
#: tools/ruledit/requirers_dlg.cpp:68
#, c-format
msgid "Needed by %s"
msgstr ""

#: tools/ruledit/ruledit.cpp:135
msgid "Print a summary of the options"
msgstr ""

#: tools/ruledit/ruledit.cpp:137
msgid "Print the version number"
msgstr ""

#. TRANS: argument (don't translate) VALUE (translate)
#: tools/ruledit/ruledit.cpp:140
msgid "ruleset RULESET"
msgstr ""

#: tools/ruledit/ruledit.cpp:141
msgid "Ruleset to use as the starting point."
msgstr ""

#: tools/ruledit/ruledit.cpp:154
msgid "Can only edit one ruleset at a time.\n"
msgstr ""

#: tools/ruledit/ruledit.cpp:161
#, c-format
msgid "Unrecognized option: \"%s\"\n"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:115
#, c-format
msgid ""
"%s%s\n"
"commit: %s"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:126
msgid "Give ruleset to use as starting point."
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:138
msgid "Start editing"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:148
msgid "Misc"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:150
msgid "Tech"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:152
msgid "Buildings"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:154
msgid "Units"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:156
msgid "Nations"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:165
msgid "Welcome to freeciv-ruledit"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:185
msgid "Ruleset loaded"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:201
msgid "Ruleset loading failed!"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:245
msgid "Freeciv Ruleset Editor"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:261
msgid "Are you sure you want to quit?"
msgstr ""

#: tools/ruledit/ruledit_qt.cpp:265
msgid "Quit?"
msgstr ""

#: tools/ruledit/tab_building.cpp:61 tools/ruledit/tab_tech.cpp:61
#: tools/ruledit/tab_unit.cpp:61
msgid "Rule Name"
msgstr ""

#: tools/ruledit/tab_building.cpp:69 tools/ruledit/tab_tech.cpp:69
#: tools/ruledit/tab_unit.cpp:69
msgid "Name"
msgstr ""

#: tools/ruledit/tab_building.cpp:152
msgid "A building with that rule name already exists!"
msgstr ""

#: tools/ruledit/tab_misc.cpp:61
msgid "Ruleset name"
msgstr ""

#: tools/ruledit/tab_misc.cpp:66
msgid "Ruleset version"
msgstr ""

#: tools/ruledit/tab_misc.cpp:71
msgid "Save to directory"
msgstr ""

#: tools/ruledit/tab_misc.cpp:78
msgid "Save now"
msgstr ""

#: tools/ruledit/tab_misc.cpp:85
msgid "?stat:Terrains"
msgstr ""

#: tools/ruledit/tab_misc.cpp:89
msgid "?stat:Resources"
msgstr ""

#: tools/ruledit/tab_misc.cpp:93
msgid "?stat:Techs"
msgstr ""

#: tools/ruledit/tab_misc.cpp:97
msgid "?stat:Unit Classes"
msgstr ""

#: tools/ruledit/tab_misc.cpp:101
msgid "?stat:Unit Types"
msgstr ""

#: tools/ruledit/tab_misc.cpp:105
msgid "?stat:Buildings"
msgstr ""

#: tools/ruledit/tab_misc.cpp:109
msgid "?stat:Nations"
msgstr ""

#: tools/ruledit/tab_misc.cpp:113
msgid "?stat:Styles"
msgstr ""

#: tools/ruledit/tab_misc.cpp:117
msgid "?stat:Specialists"
msgstr ""

#: tools/ruledit/tab_misc.cpp:121
msgid "?stat:Governments"
msgstr ""

#: tools/ruledit/tab_misc.cpp:125
msgid "?stat:Disasters"
msgstr ""

#: tools/ruledit/tab_misc.cpp:129
msgid "?stat:Achievements"
msgstr ""

#: tools/ruledit/tab_misc.cpp:133
msgid "?stat:Extras"
msgstr ""

#: tools/ruledit/tab_misc.cpp:137
msgid "?stat:Bases"
msgstr ""

#: tools/ruledit/tab_misc.cpp:141
msgid "?stat:Roads"
msgstr ""

#: tools/ruledit/tab_misc.cpp:145
msgid "?stat:Multipliers"
msgstr ""

#: tools/ruledit/tab_misc.cpp:202
msgid "Ruleset saved"
msgstr ""

#: tools/ruledit/tab_nation.cpp:49
msgid "Use nationlist"
msgstr ""

#: tools/ruledit/tab_nation.cpp:53
msgid "Nationlist"
msgstr ""

#: tools/ruledit/tab_tech.cpp:80
msgid "Req1"
msgstr ""

#: tools/ruledit/tab_tech.cpp:89
msgid "Req2"
msgstr ""

#: tools/ruledit/tab_tech.cpp:97
msgid "Root Req"
msgstr ""

#: tools/ruledit/tab_tech.cpp:182
msgid "Never"
msgstr ""

#: tools/ruledit/tab_tech.cpp:318
msgid "A tech with that rule name already exists!"
msgstr ""

#: tools/ruledit/tab_unit.cpp:152
msgid "A unit type with that rule name already exists!"
msgstr ""

#: tools/ruledit/validity.c:64
msgid "Effect"
msgstr ""

#: tools/ruledit/validity.c:125
msgid "Action Enabler"
msgstr ""

#: tools/ruledit/validity.c:145
msgid "Music Style"
msgstr ""
